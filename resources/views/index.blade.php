<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="Want to start your yoga classes website online? Download yoga studio free website html template for your classes and course. Its Free Download it now.">
<meta name="keywords" content="yoga teacher, yoga responsive, yoga instructor, yoga studio, yoga theme, yoga blog, yoga website, yoga fitness, yoga templates free download">
<title>Combo Fitness</title>
<!-- Bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- style css -->
<!-- <link rel="stylesheet" type="text/css" href="css/style3.css">  -->
<link rel="stylesheet" type="text/css" href="css/vendor.css">
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/style2.css">


    <!-- script
================================================== -->
<script src="js/modernizr.js"></script>
<script src="js/pace.min.js"></script>


<!-- animsition css -->
<link rel="stylesheet" type="text/css" href="css/animsition.min.css">
<!-- Font Awesome CSS -->
<link href="css/font-awesome.min.css" rel="stylesheet">
<!-- font css -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900" rel="stylesheet">
<!-- owl Carousel Css -->
<link href="css/owl.carousel.css" rel="stylesheet">
<link href="css/owl.theme.css" rel="stylesheet">
</head>

<body class="animsition">
    <div class="intro-section">
        <!-- intro section -->
        <div class="">
            <!-- navigation-transparent -->
            <div class="header">
                <!-- navigation -->
                <div class="container">
                <div class="row">
                <div class="col-md-3 col-sm-12">
                    <a class="logo" href="index"><img src="images/logo.png" alt=""></a>
                </div>
                <div class="col-md-8 col-sm-12">
                    <div id="navigation" class="navigation">
                        <ul class="pull-right">
                            <li class="active"><a href="home" title="Home" class="animsition-link">Home</a></li>
                            <li><a href="" title="Classes" class="animsition-link">Classes</a>
                                <ul>
                                    <li><a href="crosstraining" title="Crossfitness">Cross Fitness</a></li>
                                    <li><a href="personaltraining" title="personal">Personal</a></li>
                                    <li><a href="kickboxing" title="kickboxing">Kick Boxing</a></li>
                                
                                </ul>
                            </li>
                            <li><a href="schedule" title="schedule" class="animsition-link">Schedule</a></li>  
                            <li><a href="gallery" title="Galery" class="animsition-link">Gallery</a></li>     
                            <li><a href="contact" title="Contact Us" class="animsition-link">Contact Us</a></li>
                        </ul>
                    </div>
                </div>
            </div>
                </div>
            </div>
        </div>
        <!-- /.navigation -->
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 text-center">
                    <div class="intro-caption">
                        <!-- intro caption -->
                        <h1 class="intro-title">The Best Fitness Center</h1>
                        <p class="mb40">ABC fitness center offers you a variety of sessions to keep you fit and healthy.
                            <br> Pregnant ladies, patients, kids and sports teams can enjoy various fittness packages that suits their needs.
                        </p></div>
                    <!-- /.intro caption -->
                </div>
            </div>
        </div>
    </div>
    <!-- /.intro section -->

    <!-- welcome section -->
    <div class="section-space80 bg-light">
    <!-- section-space80 -->
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 text-center mb40">
                <h1 class="mb20">Welcome to Our Fitness Center</h1>
                <p class="lead">Our motive is to transform your lives through helping you to make your body function in an high level. Being physically active
                    brings you many benifits.Decreasing the risk of heart diseases, enriching the blood circulation and uplifting the mental health 
                    are some of them. We ensure that we can make these sessions joyful and valuable that will increase the overall happiness in your life.
                    <br><br><b>We look forward to welcoming you in to our community!</b></p>
                <a href="contact" class="btn btn-outline">Conatct Us</a>
            </div>
            <div class="col-md-offset-2 col-md-8 col-sm-12">
                <div class="video-section">
                    <!--video-section-->
                    <div class="video-img">
                        <!--video-img-->
                        <img src="images/about-img.jpg" alt="" class="img-responsive mb30"> </div>
                    <!--/.video-img-->
                    <div class="video-action">
                        <!--video-action-->
                        <a href="#" type="button" class=" " data-toggle="modal" data-target="#myModal"><img src="images/video-sign.png" alt="" class=""></a>
            
                        <!-- Button trigger modal -->
                    </div>
                    <!--/.video-action-->
                </div>
                <!--/.video-section-->
            </div>
        </div>
    </div>
</div>
<!-- /welcome section -->



<!-- motivation --> 
<div id="fh5co-services" class="fh5co-bg-section">
		<div class="container">
			<div class="row">
				<div class="col text-center animate-box">
				<h2>
                Obstacles don't have to stop you. If you run into a wall, don't turn around and give up. Figure out how to climb it, go through it, or work around it
                </h2>
				<center>-Michael Jordan-</center>
				</div>
			
				</div>
			</div>
		</div>
	</div>
<!-- /motivation --> 





<!-- classes intro section -->
<section class="s-content" >       
        <div class="row masonry-wrap" >
            <div class="masonry">

                <div class="grid-sizer"></div>

                  <article class="masonry__brick entry format-standard" data-aos=" -up">

<div class="entry__thumb">
    <a href="single-standard.html" class="entry__thumb-link">
        <img src="images/thumbs/masonry/crossfit-400.jpg" 
                srcset="images/thumbs/masonry/crossfit-400.jpg 1x, images/thumbs/masonry/crossfit-800.jpg 2x" alt="">
    </a>
</div>

<div class="entry__text">
    <div class="entry__header">
        
        <h1 class="entry__title"><a href="crosstraining">Cross Training</a></h1>
        
    </div>
    <div class="entry__excerpt">
        <p>
            Lorem ipsum Sed eiusmod esse aliqua sed incididunt aliqua incididunt mollit id et sit proident dolor nulla sed commodo est ad minim elit reprehenderit nisi officia aute incididunt velit sint in aliqua...
        </p>
    </div>
    <div >
            <br>
            <a href="crosstraining" class="btn btn-outline">More</a>
    </div>
</div>

</article> <!-- end article -->
       
        <article class="masonry__brick entry format-standard" data-aos=" -up">
                
        <div class="entry__thumb">
    <a href="single-standard.html" class="entry__thumb-link">
        <img src="images/thumbs/masonry/crossfit-400.jpg" 
                srcset="images/thumbs/masonry/crossfit-400.jpg 1x, images/thumbs/masonry/crossfit-800.jpg 2x" alt="">
    </a>
</div>

            <div class="entry__text">
                <div class="entry__header">
                    
                    <h1 class="entry__title"><a href="personaltraining">Personal Training</a></h1>
                    
                </div>
                <div class="entry__excerpt">
                    <p>
                        Lorem ipsum Sed eiusmod esse aliqua sed incididunt aliqua incididunt mollit id et sit proident dolor nulla sed commodo est ad minim elit reprehenderit nisi officia aute incididunt velit sint in aliqua...
                    </p>
                </div>
                <div >
                        <br>
                        <a href="personaltraining" class="btn btn-outline">More</a>
                </div>
               
            </div>

        </article> <!-- end article -->

        <article class="masonry__brick entry format-standard" data-aos=" -up">
                
        <div class="entry__thumb">
    <a href="single-standard.html" class="entry__thumb-link">
        <img src="images/thumbs/masonry/crossfit-400.jpg" 
                srcset="images/thumbs/masonry/crossfit-400.jpg 1x, images/thumbs/masonry/crossfit-800.jpg 2x" alt="">
    </a>
</div>
    
                <div class="entry__text">
                    <div class="entry__header">
                        
                        <h1 class="entry__title"><a href="kickboxing">Kick Boxing Training</a></h1>
                        
                    </div>
                    <div class="entry__excerpt">
                        <p>
                            Lorem ipsum Sed eiusmod esse aliqua sed incididunt aliqua incididunt mollit id et sit proident dolor nulla sed commodo est ad minim elit reprehenderit nisi officia aute incididunt velit sint in aliqua...
                        </p>
                    </div>
                    <div >
                            <br>
                            <a href="kickboxing" class="btn btn-outline">More</a>
                    </div>
                   
                </div>
    
            </article> <!-- end article -->

      

<article class="masonry__brick entry format-standard" data-aos=" -up">
                
                <div class="entry__thumb">
                <a href="single-standard.html" class="entry__thumb-link">
                    <img src="images/thumbs/masonry/crossfit-400.jpg" 
                            srcset="images/thumbs/masonry/crossfit-400.jpg 1x, images/thumbs/masonry/crossfit-800.jpg 2x" alt="">
                </a>
            </div>

            <div class="entry__text">
                <div class="entry__header">
                    
                    <h1 class="entry__title"><a href="yoga">Yoga</a></h1>
                    
                </div>
                <div class="entry__excerpt">
                    <p>
                        Lorem ipsum Sed eiusmod esse aliqua sed incididunt aliqua incididunt mollit id et sit proident dolor nulla sed commodo est ad minim elit reprehenderit nisi officia aute incididunt velit sint in aliqua...
                    </p>
                </div>
                <div >
                        <br>
                        <a href="yoga" class=" btn btn-outline ">More</a>
                </div>
            </div>

        </article> <!-- end article -->

            </div> <!-- end masonry -->
        </div> <!-- end masonry-wrap -->
</section>
<!--/classes intro section -->

<!--trainers section -->
<div class="section-space60 team" id="team">
        <div class="container" id="trainers">
        <div class="team-head text-center" >
        <h3>TRAINERS</h3>
        <span> </span>
    </div>
    <div class="team-grids">
        <div class="col-md-3 team-grid">
            <a data-toggle="modal" data-target=".bs-example-modal-md" href="#" class="b-link-stripe b-animate-go  thickbox">
             <img class="p-img" src="images/t1.jpg" /><div class="b-wrapper">
                 <h2 class="b-animate b-from-left    b-delay03 ">
                     <div class="animate-head">
                         <div class="animate-head-left">
                             <h3>Duis nec congue</h3>
                             <span>Neque porro quisquam est qui dolorem </span>
                         </div>
                         <div class="clearfix"> </div>
                     </div>
                 </h2>
              </div></a>
              <div class="t-member-info">
                  <h5>Otto W. Schreffler</h5>
                  <span>trainer</span>
              </div>
        </div>
        <div class="col-md-3 team-grid">
            <a data-toggle="modal" data-target=".bs-example-modal-md" href="#" class="b-link-stripe b-animate-go  thickbox">
             <img class="p-img" src="images/t2.jpg" /><div class="b-wrapper">
                 <h2 class="b-animate b-from-left    b-delay03 ">
                     <div class="animate-head">
                         <div class="animate-head-left">
                             <h3>Duis nec congue</h3>
                             <span>Neque porro quisquam est qui dolorem </span>
                         </div>
                         <div class="clearfix"> </div>
                     </div>
                 </h2>
              </div></a>
              <div class="t-member-info">
                  <h5>Brooke C. Carrington</h5>
                  <span>trainer</span>
              </div>
        </div>
        <div class="col-md-3 team-grid">
            <a data-toggle="modal" data-target=".bs-example-modal-md" href="#" class="b-link-stripe b-animate-go  thickbox">
             <img class="p-img" src="images/t3.jpg" /><div class="b-wrapper">
                 <h2 class="b-animate b-from-left    b-delay03 ">
                     <div class="animate-head">
                         <div class="animate-head-left">
                             <h3>Duis nec congue</h3>
                             <span>Neque porro quisquam est qui dolorem </span>
                         </div>
                         <div class="clearfix"> </div>
                     </div>
                 </h2>
              </div></a>
              <div class="t-member-info">
                  <h5>Arthur M. Huffman</h5>
                  <span>trainer</span>
              </div>
        </div>
        <div class="col-md-3 team-grid">
            <a data-toggle="modal" data-target=".bs-example-modal-md" href="#" class="b-link-stripe b-animate-go  thickbox">
             <img class="p-img" src="images/t4.jpg" /><div class="b-wrapper">
                 <h2 class="b-animate b-from-left    b-delay03 ">
                     <div class="animate-head">
                         <div class="animate-head-left">
                             <h3>Bernice B. Work</h3>
                             <span>Neque porro quisquam est qui dolorem </span>
                         </div>
                         <div class="clearfix"> </div>
                     </div>
                 </h2>
              </div></a>
              <div class="t-member-info">
                  <h5>Otto W. Schreffler</h5>
                  <span>trainer</span>
              </div>
        </div>
    </div>
        </div>
    </div>
<!--/trainers section -->



 
<!--/classes section -->

    <div class="footer section-space60">
        <!-- footer -->
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4">
                    <div class="footer-logo"> <img src="images/logo.png" alt="" class=""> </div>
                </div>
                <div class="col-md-9 col-sm-8">
                <p class="mb40">ABC fitness center offers you a variety of sessions to keep you fit and healthy.
                <br> Pregnant ladies, patients, kids and sports teams can enjoy various fittness packages that suits their needs.
            </p>
                </div>
            </div>
            <hr>
            <div class="row">
            <div class="col-md-2 col-sm-2">
                    
                    <!-- /.footer-widget -->
                </div>
                <div class="col-md-3 col-sm-4">
                    <div class="footer-widget">
                        <!-- footer-widget -->
                        <i class="fa fa-map-marker mb30"></i>
                        <h3 class="footer-title">Locations</h3>
                        <address>
                            NO: 12
                            <br> Maharagama, Sri Lanka
                        </address>
                        <ul class="listnone contact">
                            <li>+91-123-456-789</li>
                            <li>+91-456-369-964</li>
                        </ul>
                    </div>
                    <!-- /.footer-widget -->
                </div>
                <div class="col-md-3 col-sm-4">
                    <div class="footer-widget">
                        <!-- footer-widget -->
                        <i class="fa fa-clock-o mb30"></i>
                        <h3 class="footer-title">Opening Hour</h3>
                        <ul class="listnone mb0 no-padding">
                            <li>Mon: 6:00am to 9:00pm</li>
                            <li>Tues - Fri: 8:00am to 7:00pm</li>
                            <li>Sat - Sun: 9:00am 1:00pm </li>
                        </ul>
                    </div>
                    <!-- /.footer-widget -->
                </div>
                <div class="col-md-2 col-sm-4">
                    <div class="footer-widget">
                        <!-- footer-widget -->
                        <i class="fa fa-mail-forward mb30"></i>
                        <h3 class="footer-title">Follow Us On</h3>
                        <ul class="listnone no-padding mb0">
                            <li class="footer-link"><a href="#"><i class="fa fa-facebook-f"></i> facebook</a></li>
                            <li class="footer-link"><a href="#"><i class="fa fa-twitter"></i> twitter</a></li>
                        </ul>
                    </div>
                    <!-- /.footer-widget -->
                </div>
                
            </div>
        </div>
    </div>
    <!-- /.footer -->
    <div class="tiny-footer">
        <!-- tiny-footer -->
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <p>ABC Fittness Center © Copy Rights 2017. All Rights Reserved.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- /.tiny-footer -->
    <!-- back to top icon -->
    <a href=" #0 " class="cd-top" title="Go to top">Top</a>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content ">
                <div class="modal-body">
                    <iframe width="100%" height="600" src="https://www.youtube.com/embed/dy2UQofvwH8\&t=6s" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/menumaker.js"></script>
    <!-- animsition -->
    <script type="text/javascript" src="js/animsition.js"></script>
    <script type="text/javascript" src="js/animsition-script.js"></script>
    <!-- sticky header -->
    <script type="text/javascript" src="js/jquery.sticky.js"></script>
    <script type="text/javascript" src="js/sticky-header.js"></script>
    <!-- owl carsoul -->
    <script type="text/javascript" src="js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="js/testimonial.js"></script>
    <!-- Back to top script -->
    <script src="js/back-to-top.js" type="text/javascript"></script>

        <!-- Java Script
    ================================================== -->
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>

</body>

</html>
