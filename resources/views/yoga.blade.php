<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Want to start your yoga classes website online? Download yoga studio free website html template for your classes and course. Its Free Download it now.">
    <meta name="keywords" content="yoga teacher, yoga responsive, yoga instructor, yoga studio, yoga theme, yoga blog, yoga website, yoga fitness, yoga templates free download">
    <title>Yoga</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- style css -->
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <!-- animsition css -->
    <link rel="stylesheet" type="text/css" href="css/animsition.min.css">
    <!-- Font Awesome CSS -->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <!-- font css -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900" rel="stylesheet">
    <!-- owl Carousel Css -->
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/owl.theme.css" rel="stylesheet">

	
	<!-- Animate.css -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="css/magnific-popup.css">

	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="css/style4.css">

	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>


</head>

<body class="animsition">
<div class="intro-section-yoga">
<!-- intro section -->
<div class="">
	<!-- navigation-transparent -->
	<div class="header">
		<!-- navigation -->
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-12">
					<a class="logo" href="index"><img src="images/logo.png" alt=""></a>
				</div>
				<div class="col-md-8 col-sm-12">
					<div id="navigation" class="navigation">
						<ul class="pull-right">
							<li class="active"><a href="index" title="Home" class="animsition-link">Home</a></li>
							<li><a href="" title="Classes" class="animsition-link">Classes</a>
								<ul>
									<li><a href="crosstraining" title="Crossfitness">Cross Fitness</a></li>
                                    <li><a href="personaltraining" title="personal">Personal</a></li>
                                    <li><a href="kickboxing" title="kickboxing">Kick Boxing</a></li>
                                    <li><a href="yoga" title="yoga">Yoga</a></li>
								</ul>
							</li>   
							<li><a href="schedule" title="schedule" class="animsition-link">Schedule</a></li> 
							<li><a href="gallery" title="Galeery" class="animsition-link">Gallery</a></li>     
                            <li><a href="contact" title="Contact Us" class="animsition-link">Contact Us</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /.navigation -->
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 text-center">
                    <div class="intro-caption">
                        <!-- intro caption -->
                        <h1 class="intro-title">Give this world good energy with YOGA</h1>
                        <p class="mb40">
							<br> 
							<br>
							
							</p>
                         </div>
                    <!-- /.intro caption -->
                </div>
            </div>
        </div>
	</div>
	
<!-- trainers --> 
	<div id="fh5co-services" class="fh5co-bg-section">
		<div class="container">
			<div class="row">
				<div class="col-md-4 text-center animate-box">
					<div class="services">
					<span><img class="img-responsive fh5co-rounded-images" src="images/t1.jpg" alt=""></span>
					<h3>Trainer 1</h3>
					<p> Qualifications : </p>
					<p> Email address : </p>
					<p> Contact number : </p>
					</div>
				</div>
				<div class="col-md-4 text-center animate-box">
					<div class="services">
						<span><img class="img-responsive fh5co-rounded-images" src="images/t2.jpg" alt=""></span>
						<h3>Trainer 2</h3>
						<p> Qualifications : </p>
						<p> Email address : </p>
						<p> Contact number : </p>
						</div>
				</div>
				<div class="col-md-4 text-center animate-box">
					<div class="services">
						<span><img class="img-responsive fh5co-rounded-images" src="images/t3.jpg" alt=""></span>
						<h3>Trainer 3</h3>
						<p> Qualifications : </p>
						<p> Email address : </p>
						<p> Contact number : </p>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- /trainers --> 

<!-- benefits --> 
<div id="fh5co-pricing">
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
					<h2>Yoga Benifits</h2>
					<p> Yoga is outperformed aerobic exercises with balancing, flexibility , energy and strength.  </p>
				</div>
			</div>
			<div class="row">
				<div class="container">
					<div class="col-md-12 animate-box">
						<div class="price-box">
							<h2 ><b>Benifits</b></h2>
							<ul class="classes">
								<li>  Weight loss </li>
								<li class="color"> Stress Relief</li>
								<li>Increase Energy </li>
								<li class="color">prevent cartilage and joint breakdown</li>
								<li>Increase the blood flow</li>
								<li class="color">increase flexibility</li>
								<li>Cardio and circulatory health</li>
							</ul>	
					</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<!-- /benefits --> 

<!-- 
<div class="price-box">
								<div class="progress">
								<div class="progress-bar" role="progressbar" style="width: 75%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
								</div>
								<div class="progress">
								<div class="progress-bar" role="progressbar" style="width: 55%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
								</div>
								<div class="progress">
								<div class="progress-bar" role="progressbar" style="width: 25%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
								</div>
								<div class="progress">
								<div class="progress-bar" role="progressbar" style="width: 5%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
								</div>
									<div class="progress">
								<div class="progress-bar" role="progressbar" style="width: 15%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
								</div>
							</div> -->


<!-- class schedule --> 
<div id="fh5co-schedule" class="fh5co-bg" style="background-image: url(images/img_bg_1.jpg);">
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
			<h2>Class Schedule</h2>
		</div>
	</div>

	<div class="row animate-box">
		
		<div class="fh5co-tabs">
			

			<!-- Tabs -->
			<div class="fh5co-tab-content-wrap">
				<div class="fh5co-tab-content tab-content active" data-tab-content="1">
					<ul class="class-schedule">
						<li class="text-center">
							
							
							<span class="time">Sunday</span>
							<h4>Time</h4>
							<small>Trainer Name</small>
						</li>
						<li class="text-center">
							
							<span class="time">Monday</span>
							<h4>Time</h4>
							<small>Trainer Name</small>
						</li>
						<li class="text-center">
							
							<span class="time">Tuesday</span>
							<h4>Time</h4>
							<small>Trainer Name</small>
						</li>
						<li class="text-center">
							
							<span class="time">Wednesday</span>
							<h4>Time</h4>
							<small>Trainer Name</small>
						</li>
						<li class="text-center">
							
							<span class="time">Thursday</span>
							<h4>Time</h4>
							<small>Trainer Name</small>
						</li>
						<li class="text-center">
						
							<span class="time">Friday</span>
							<h4>Time</h4>
							<small>Trainer Name</small>
						</li>
						<li class="text-center">
							
							<span class="time">Saturday</span>
							<h4>Time</h4>
							<small>Trainer Name</small>
						</li>
						<li class="text-center">
							
							<span class="time">Special Events</span>
							<h4>Time</h4>
							<small>Trainer Name</small>
						</li>
						
					</ul>
				</div>			

			</div>

		</div>
	</div>
</div>
</div>
<!-- /class schedule --> 



<!-- gallery--> 
<div id="fh5co-gallery">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading animate-box">
					<h1>Gallery</h1>
					
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<div class="row row-bottom-padded-md">
				<div class="col-md-12">
					<ul id="fh5co-portfolio-list">

						<li class="one-third animate-box" data-animate-effect="fadeIn" style="background-image: url(images/yoga1.jpg); ">
							<a href="#">
								<div class="case-studies-summary">
									<span>Yoga</span>
									<h2> Inspiration</h2>
								</div>
							</a>
						</li>
						<li class="two-third animate-box" data-animate-effect="fadeIn" style="background-image: url(images/yoga2.jpg); ">
							<a href="#">
								<div class="case-studies-summary">
									<span>As a team</span>
									<h2>Good exercises make you better</h2>
								</div>
							</a>
						</li>

						<li class="one-third animate-box" data-animate-effect="fadeIn" style="background-image: url(images/yoga3.jpg); ">
							<a href="#">
								<div class="case-studies-summary">
									<span>Positive Mind</span>
									<h2>Best guidance</h2>
								</div>
							</a>
						</li>

						<li class="two-third animate-box" data-animate-effect="fadeIn" style="background-image: url(images/yoga4.jpg); "> 
							<a href="#">
								<div class="case-studies-summary">
									<span>Meditation</span>
									<h2>Timer starts now!</h2>
								</div>
							</a>
						</li>
						<li class="two-third animate-box" data-animate-effect="fadeIn" style="background-image: url(images/yoga5.jpg); ">
							<a href="#">
								<div class="case-studies-summary">
									<span>Meditation</span>
									<h2> Fat Burning</h2>
								</div>
							</a>
						</li>
						<li class="one-third animate-box" data-animate-effect="fadeIn" style="background-image: url(images/yoga6.jpg); ">
							<a href="#">
								<div class="case-studies-summary">
									<span> Self motivated</span>
									<h2> trength, serenity and bliss</h2>
								</div>
							</a>
						</li>					
						<li class="two-third animate-box" data-animate-effect="fadeIn" style="background-image: url(images/yoga7.jpg); ">
							<a href="#">
								<div class="case-studies-summary">
									<span>tolarating</span>
									<h2>Heal the mind</h2>
								</div>
							</a>
						</li>
						<li class="one-third animate-box" data-animate-effect="fadeIn" style="background-image: url(images/yoga8.jpg); ">
							<a href="#">
								<div class="case-studies-summary">
									<span>Balancing</span>
									<h2>Life is about Balancing</h2>
								</div>
							</a>
						</li>	
				
				
				
				<li class="two-third animate-box" data-animate-effect="fadeIn" style="background-image: url(images/gallery-4.jpg); "> 
							<a href="#">
								<div class="case-studies-summary">
									<span></span>
									<h2>Videos</h2>
								</div>
							</a>
						</li>
						<li class="two-third animate-box" data-animate-effect="fadeIn" style="background-image: url(images/gallery-3.jpg); ">
							<a href="#">
								<div class="case-studies-summary">
									<span></span>
									<h2>Videos</h2>
								</div>
							</a>
						</li>
					</ul>	
					
				</div>
			</div>
		</div>
	</div>
	
<!-- /gallery--> 

<div class="footer section-space60">
        <!-- footer -->
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4">
                    <div class="footer-logo"> <img src="images/logo.png" alt="" class=""> </div>
                </div>
                <div class="col-md-9 col-sm-8">
                <p class="mb40">ABC fittness center offers you a variety of sessions to keep you fit and healthy.
                <br> Pregnant ladies, patients, kids and sports teams can enjoy various fittness packages that suits their needs.
            </p>
                </div>
            </div>
            <hr>
            <div class="row">
            <div class="col-md-2 col-sm-2">
                    
                    <!-- /.footer-widget -->
                </div>
                <div class="col-md-3 col-sm-4">
                    <div class="footer-widget">
                        <!-- footer-widget -->
                        <i class="fa fa-map-marker mb30"></i>
                        <h3 class="footer-title">Locations</h3>
                        <address>
                            NO: 12
                            <br> Maharagama, Sri Lanka
                        </address>
                        <ul class="listnone contact">
                            <li>+91-123-456-789</li>
                            <li>+91-456-369-964</li>
                        </ul>
                    </div>
                    <!-- /.footer-widget -->
                </div>
                <div class="col-md-3 col-sm-4">
                    <div class="footer-widget">
                        <!-- footer-widget -->
                        <i class="fa fa-clock-o mb30"></i>
                        <h3 class="footer-title">Opening Hour</h3>
                        <ul class="listnone mb0 no-padding">
                            <li>Mon: 6:00am to 9:00pm</li>
                            <li>Tues - Fri: 8:00am to 7:00pm</li>
                            <li>Sat - Sun: 9:00am 1:00pm </li>
                        </ul>
                    </div>
                    <!-- /.footer-widget -->
                </div>
                <div class="col-md-2 col-sm-4">
                    <div class="footer-widget">
                        <!-- footer-widget -->
                        <i class="fa fa-mail-forward mb30"></i>
                        <h3 class="footer-title">Follow Us On</h3>
                        <ul class="listnone no-padding mb0">
                            <li class="footer-link"><a href="#"><i class="fa fa-facebook-f"></i> facebook</a></li>
                            <li class="footer-link"><a href="#"><i class="fa fa-twitter"></i> twitter</a></li>
                        </ul>
                    </div>
                    <!-- /.footer-widget -->
                </div>
                
            </div>
        </div>
    </div>
   <!-- /.footer -->
   <div class="tiny-footer">
        <!-- tiny-footer -->
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <p>ABC Fittness Center © Copy Rights 2017. All Rights Reserved.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- /.tiny-footer -->
    <!-- back to top icon -->
    <a href=" #0 " class="cd-top" title="Go to top">Top</a>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content ">
                <div class="modal-body">
                    <iframe width="100%" height="600" src="https://www.youtube.com/embed/dy2UQofvwH8\&t=6s" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/menumaker.js"></script>
    <!-- animsition -->
    <script type="text/javascript" src="js/animsition.js"></script>
    <script type="text/javascript" src="js/animsition-script.js"></script>
    <!-- sticky header -->
    <script type="text/javascript" src="js/jquery.sticky.js"></script>
    <script type="text/javascript" src="js/sticky-header.js"></script>
    <!-- owl carsoul -->
    <script type="text/javascript" src="js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="js/testimonial.js"></script>
    <!-- Back to top script -->
	<script src="js/back-to-top.js" type="text/javascript"></script>
	
	
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<!-- Stellar Parallax -->
	<script src="js/jquery.stellar.min.js"></script>
	<!-- countTo -->
	<script src="js/jquery.countTo.js"></script>
	<!-- Magnific Popup -->
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/magnific-popup-options.js"></script>
	<!-- Main -->
	<script src="js/main2.js"></script>




</body>

</html>
