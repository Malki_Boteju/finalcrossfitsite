<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Want to start your yoga classes website online? Download yoga studio free website html template for your classes and course. Its Free Download it now.">
    <meta name="keywords" content="yoga teacher, yoga responsive, yoga instructor, yoga studio, yoga theme, yoga blog, yoga website, yoga fitness, yoga templates free download">
    <title>Contact US</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- style css -->
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <!-- animsition css -->
    <link rel="stylesheet" type="text/css" href="css/animsition.min.css">
    <!-- Font Awesome CSS -->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <!-- font css -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900" rel="stylesheet">
    <!-- owl Carousel Css -->
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/owl.theme.css" rel="stylesheet">

	
	<!-- Animate.css -->
	<link rel="stylesheet" href="css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="css/bootstrap.css">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="css/magnific-popup.css">

	<!-- Owl Carousel  -->
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="css/style4.css">

	<!-- Modernizr JS -->
	<script src="js/modernizr-2.6.2.min.js"></script>


</head>

<body class="animsition">
<div class="intro-section-contact">
<!-- intro section -->
<div class="">
	<!-- navigation-transparent -->
	<div class="header">
		<!-- navigation -->
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-12">
					<a class="logo" href="index"><img src="images/logo.png" alt=""></a>
				</div>
				<div class="col-md-8 col-sm-12">
					<div id="navigation" class="navigation">
						<ul class="pull-right">
							<li class="active"><a href="index" title="Home" class="animsition-link">Home</a></li>
							<li><a href="" title="Classes" class="animsition-link">Classes</a>
								<ul>
									<li><a href="crosstraining" title="Crossfitness">Cross Fitness</a></li>
                                    <li><a href="personaltraining" title="personal">Personal</a></li>
                                    <li><a href="kickboxing" title="kickboxing">Kick Boxing</a></li>
                                    <li><a href="yoga" title="yoga">Yoga</a></li>
								</ul>
							</li>   
							<li><a href="schedule" title="schedule" class="animsition-link">Schedule</a></li> 
							<li><a href="gallery" title="Galeery" class="animsition-link">Gallery</a></li>     
                            <li><a href="contact" title="Contact Us" class="animsition-link">Contact Us</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /.navigation -->
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 text-center">
                    <div class="intro-caption">
                        <!-- intro caption -->
                        <h1 class="intro-title">Contact Us</h1>
                        <p class="mb40">
							<br> 
							<br>
							
							</p>
                         </div>
                    <!-- /.intro caption -->
                </div>
            </div>
        </div>
	</div>
	

<!-- enquiry --> 
<div id="fh5co-services" class="fh5co-bg-section">
		<div class="container">
			<div class="row">
				<div class="col text-center animate-box">
					<div class="services">
					<h3>
					Do you have a question that is not answered on our website? Or would you like to make a general enquiry? Whatever it may be, please use the form below so that we can help assist you.
					</h3>
					</div>
				</div>
			
				</div>
			</div>
		</div>
	</div>
<!-- /enquiry --> 

<!-- trainers --> 
	<div id="fh5co-services" class="fh5co-bg-section">
		<div class="container">
			<div class="row">
				<div class="col text-center animate-box">
					<div class="services">

					<section id="contact">
  <div class="container">
	
	<div class="row">
	  <div class="col-md-7">
        <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d3736489.7218514383!2d90.21589792292741!3d23.857125486636733!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1506502314230" width="100%" height="315" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>

      <div class="col-md-4">
          <h4><strong>Get in Touch</strong></h4>
        <form>
          <div class="form-group">
            <input type="text" class="form-control" name="" value="" placeholder="Name">
          </div>
          <div class="form-group">
            <input type="email" class="form-control" name="" value="" placeholder="E-mail">
          </div>
          <div class="form-group">
            <input type="tel" class="form-control" name="" value="" placeholder="Phone">
          </div>
          <div class="form-group">
            <textarea class="form-control" name="" rows="3" placeholder="Message"></textarea>
          </div>
          <button class="btn btn-default" type="submit" name="button">
              <i class="fa fa-paper-plane-o" aria-hidden="true"></i> Submit
          </button>
        </form>
      </div>
    </div>
  </div>
</section>
					</div>
				</div>
			
				</div>
			</div>
		</div>
	</div>
<!-- /trainers --> 






<div class="footer section-space60">
        <!-- footer -->
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4">
                    <div class="footer-logo"> <img src="images/logo.png" alt="" class=""> </div>
                </div>
                <div class="col-md-9 col-sm-8">
                <p class="mb40">ABC fittness center offers you a variety of sessions to keep you fit and healthy.
                <br> Pregnant ladies, patients, kids and sports teams can enjoy various fittness packages that suits their needs.
            </p>
                </div>
            </div>
            <hr>
            <div class="row">
            <div class="col-md-2 col-sm-2">
                    
                    <!-- /.footer-widget -->
                </div>
                <div class="col-md-3 col-sm-4">
                    <div class="footer-widget">
                        <!-- footer-widget -->
                        <i class="fa fa-map-marker mb30"></i>
                        <h3 class="footer-title">Locations</h3>
                        <address>
                            NO: 12
                            <br> Maharagama, Sri Lanka
                        </address>
                        <ul class="listnone contact">
                            <li>+91-123-456-789</li>
                            <li>+91-456-369-964</li>
                        </ul>
                    </div>
                    <!-- /.footer-widget -->
                </div>
                <div class="col-md-3 col-sm-4">
                    <div class="footer-widget">
                        <!-- footer-widget -->
                        <i class="fa fa-clock-o mb30"></i>
                        <h3 class="footer-title">Opening Hour</h3>
                        <ul class="listnone mb0 no-padding">
                            <li>Mon: 6:00am to 9:00pm</li>
                            <li>Tues - Fri: 8:00am to 7:00pm</li>
                            <li>Sat - Sun: 9:00am 1:00pm </li>
                        </ul>
                    </div>
                    <!-- /.footer-widget -->
                </div>
                <div class="col-md-2 col-sm-4">
                    <div class="footer-widget">
                        <!-- footer-widget -->
                        <i class="fa fa-mail-forward mb30"></i>
                        <h3 class="footer-title">Follow Us On</h3>
                        <ul class="listnone no-padding mb0">
                            <li class="footer-link"><a href="#"><i class="fa fa-facebook-f"></i> facebook</a></li>
                            <li class="footer-link"><a href="#"><i class="fa fa-twitter"></i> twitter</a></li>
                        </ul>
                    </div>
                    <!-- /.footer-widget -->
                </div>
                
            </div>
        </div>
    </div>
   <!-- /.footer -->
   <div class="tiny-footer">
        <!-- tiny-footer -->
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <p>ABC Fittness Center © Copy Rights 2017. All Rights Reserved.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- /.tiny-footer -->
    <!-- back to top icon -->
    <a href=" #0 " class="cd-top" title="Go to top">Top</a>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content ">
                <div class="modal-body">
                    <iframe width="100%" height="600" src="https://www.youtube.com/embed/dy2UQofvwH8\&t=6s" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/menumaker.js"></script>
    <!-- animsition -->
    <script type="text/javascript" src="js/animsition.js"></script>
    <script type="text/javascript" src="js/animsition-script.js"></script>
    <!-- sticky header -->
    <script type="text/javascript" src="js/jquery.sticky.js"></script>
    <script type="text/javascript" src="js/sticky-header.js"></script>
    <!-- owl carsoul -->
    <script type="text/javascript" src="js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="js/testimonial.js"></script>
    <!-- Back to top script -->
	<script src="js/back-to-top.js" type="text/javascript"></script>
	
	
	<!-- jQuery Easing -->
	<script src="js/jquery.easing.1.3.js"></script>
	<!-- Waypoints -->
	<script src="js/jquery.waypoints.min.js"></script>
	<!-- Stellar Parallax -->
	<script src="js/jquery.stellar.min.js"></script>
	<!-- countTo -->
	<script src="js/jquery.countTo.js"></script>
	<!-- Magnific Popup -->
	<script src="js/jquery.magnific-popup.min.js"></script>
	<script src="js/magnific-popup-options.js"></script>
	<!-- Main -->
	<script src="js/main2.js"></script>




</body>

</html>
