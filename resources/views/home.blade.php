<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Want to start your yoga classes website online? Download yoga studio free website html template for your classes and course. Its Free Download it now.">
    <meta name="keywords" content="yoga teacher, yoga responsive, yoga instructor, yoga studio, yoga theme, yoga blog, yoga website, yoga fitness, yoga templates free download">
    <title>ABC fittness center</title>
    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- style css -->
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/style2.css">
    <link rel="stylesheet" type="text/css" href="css/style3.css">
    <link rel="stylesheet" type="text/css" href="css/vendor.css">
    
    
        <!-- script
    ================================================== -->
    <script src="js/modernizr.js"></script>
    <script src="js/pace.min.js"></script>


    <!-- animsition css -->
    <link rel="stylesheet" type="text/css" href="css/animsition.min.css">
    <!-- Font Awesome CSS -->
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <!-- font css -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900" rel="stylesheet">
    <!-- owl Carousel Css -->
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/owl.theme.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="animsition">
    <div class="intro-section">
        <!-- intro section -->
        <div class="">
            <!-- navigation-transparent -->
            <div class="header">
                <!-- navigation -->
                <div class="container">
                    <div class="row">
                        <div class="col-md-3 col-sm-12">
                            <a class="logo" href="index.html"><img src="images/logo.png" alt=""></a>
                        </div>
                        <div class="col-md-8 col-sm-12">
                            <div id="navigation" class="navigation">
                                <ul class="pull-right">
                                    <li class="active"><a href="index.html" title="Home" class="animsition-link">Home</a></li>
                                    <li><a href="service-list.html" title="Classes" class="animsition-link">Classes</a>
                                        <ul>
                                            <li><a href="service-list.html" title="Yoga">Yoga</a></li>
                                            <li><a href="service-detail.html" title="Physio">Physio</a></li>
                                            <li><a href="service-detail.html" title="Zumba">Zumba</a></li>
                                            <li><a href="service-detail.html" title="Cross Fit">Cross Fit</a></li>
                                        </ul>
                                    </li>   
                                    <li><a href="testimonials.html" title="Galeery" class="animsition-link">Gallery</a></li>     
                                    <li><a href="contact-us.html" title="Contact Us" class="animsition-link">Contact Us</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.navigation -->
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 text-center">
                    <div class="intro-caption">
                        <!-- intro caption -->
                        <h1 class="intro-title">The Best Fittness Center</h1>
                        <p class="mb40">ABC fittness center offers you a variety of sessions to keep you fit and healthy.
                            <br> Pregnant ladies, patients, kids and sports teams can enjoy various fittness packages that suits their needs.
                        </p>
                        <a href="#" class="btn btn-default btn-lg">make an appointment</a> </div>
                    <!-- /.intro caption -->
                </div>
            </div>
        </div>
    </div>
   
    <div class="section-space80 bg-light">
        <!-- section-space80 -->
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 text-center mb40">
                    <h1 class="mb20">Welcome to Our Fittness Center</h1>
                    <p class="lead">Our motive is to transform your lives through helping you to make your body function in an high level. Being physically active
                        brings you many benifits.Decreasing the risk of heart diseases, enriching the blood circulation and uplifting the mental health 
                        are some of them. We ensure that we can make these sessions joyful and valuable that will increase the overall happiness in your life.
                        <br><br><b>We look forward to welcoming you in to our community!</b></p>
                    <a href="#" class="btn btn-outline">Conatct Us</a>
                </div>
                <div class="col-md-offset-2 col-md-8 col-sm-12">
                    <div class="video-section">
                        <!--video-section-->
                        <div class="video-img">
                            <!--video-img-->
                            <img src="images/about-img.jpg" alt="Hair-Care Website Template" class="img-responsive mb30"> </div>
                        <!--/.video-img-->
                        <div class="video-action">
                            <!--video-action-->
                            <a href="#" type="button" class=" " data-toggle="modal" data-target="#myModal"><img src="images/video-sign.png" alt="Hair-Care Website Template" class=""></a>
                            <!-- Button trigger modal -->
                        </div>
                        <!--/.video-action-->
                    </div>
                    <!--/.video-section-->
                </div>
            </div>
        </div>
    </div>


 
     <!-- /.intro section -->
     <div class="section-space80">
        <!-- section-space80 -->
        <div class="container" >
        <div class="row">
                <div class="col-md-12 col-sm-12  text-center mb40">  
    
    <div class="row masonry-wrap">
        <div class="masonry">

            <div class="grid-sizer"></div>

            <article class="masonry__brick entry format-standard" data-aos="fade-up">
                    
                <div class="entry__thumb">
                    <a href="single-standard.html" class="entry__thumb-link">
                        <img src="images/thumbs/masonry/lamp-400.jpg" 
                                srcset="images/thumbs/masonry/lamp-400.jpg 1x, images/thumbs/masonry/lamp-800.jpg 2x" alt="">
                    </a>
                </div>

                <div class="entry__text">
                    <div class="entry__header">
                        
                        <div class="entry__date">
                            <a href="single-standard.html">December 15, 2017</a>
                        </div>
                        <h1 class="entry__title"><a href="single-standard.html">Just a Standard Format Post.</a></h1>
                        
                    </div>
                    <div class="entry__excerpt">
                        <p>
                            Lorem ipsum Sed eiusmod esse aliqua sed incididunt aliqua incididunt mollit id et sit proident dolor nulla sed commodo est ad minim elit reprehenderit nisi officia aute incididunt velit sint in aliqua...
                        </p>
                    </div>
                    <div class="entry__meta">
                        <span class="entry__meta-links">
                            <a href="category.html">Design</a> 
                            <a href="category.html">Photography</a>
                        </span>
                    </div>
                </div>

            </article> <!-- end article -->

            <article class="masonry__brick entry format-quote" data-aos="fade-up">
                    
                <div class="entry__thumb">
                    <blockquote>
                            <p>Good design is making something intelligible and memorable. Great design is making something memorable and meaningful.</p>

                            <cite>Dieter Rams</cite>
                    </blockquote>
                </div>   

            </article> <!-- end article -->

            <article class="masonry__brick entry format-standard" data-aos="fade-up">
                    
                <div class="entry__thumb">
                    <a href="single-standard.html" class="entry__thumb-link">
                        <img src="images/thumbs/masonry/tulips-400.jpg" 
                                srcset="images/thumbs/masonry/tulips-400.jpg 1x, images/thumbs/masonry/tulips-800.jpg 2x" alt="">
                    </a>
                </div>

                <div class="entry__text">
                    <div class="entry__header">
                        
                        <div class="entry__date">
                            <a href="single-standard.html">December 15, 2017</a>
                        </div>
                        <h1 class="entry__title"><a href="single-standard.html">10 Interesting Facts About Caffeine.</a></h1>
                        
                    </div>
                    <div class="entry__excerpt">
                        <p>
                            Lorem ipsum Sed eiusmod esse aliqua sed incididunt aliqua incididunt mollit id et sit proident dolor nulla sed commodo est ad minim elit reprehenderit nisi officia aute incididunt velit sint in aliqua...
                        </p>
                    </div>
                    <div class="entry__meta">
                        <span class="entry__meta-links">
                            <a href="category.html">Health</a>
                        </span>
                    </div>
                </div>

            </article> <!-- end article -->

            <article class="masonry__brick entry format-standard" data-aos="fade-up">

                <div class="entry__thumb">
                    <a href="single-standard.html" class="entry__thumb-link">
                        <img src="images/thumbs/masonry/cookies-400.jpg" 
                                srcset="images/thumbs/masonry/cookies-400.jpg 1x, images/thumbs/masonry/cookies-800.jpg 2x" alt="">
                    </a>
                </div>

                <div class="entry__text">
                    <div class="entry__header">
                        
                        <div class="entry__date">
                            <a href="single-standard.html">December 10, 2017</a>
                        </div>
                        <h1 class="entry__title"><a href="single-standard.html">No Sugar Oatmeal Cookies.</a></h1>
                        
                    </div>
                    <div class="entry__excerpt">
                        <p>
                            Lorem ipsum Sed eiusmod esse aliqua sed incididunt aliqua incididunt mollit id et sit proident dolor nulla sed commodo est ad minim elit reprehenderit nisi officia aute incididunt velit sint in aliqua...
                        </p>
                    </div>
                    <div class="entry__meta">
                        <span class="entry__meta-links">
                            <a href="category.html">Cooking</a>
                            <a href="category.html">Health</a>
                        </span>
                    </div>
                </div>

            </article> <!-- end article -->

            <article class="masonry__brick entry format-standard" data-aos="fade-up">

                <div class="entry__thumb">
                    <a href="single-standard.html" class="entry__thumb-link">
                        <img src="images/thumbs/masonry/wheel-400.jpg" 
                                srcset="images/thumbs/masonry/wheel-400.jpg 1x, images/thumbs/masonry/wheel-800.jpg 2x" alt="">
                    </a>
                </div>

                <div class="entry__text">
                    <div class="entry__header">
                        
                        <div class="entry__date">
                            <a href="single-standard.html">December 10, 2017</a>
                        </div>
                        <h1 class="entry__title"><a href="single-standard.html">Visiting Theme Parks Improves Your Health.</a></h1>
                        
                    </div>
                    <div class="entry__excerpt">
                        <p>
                            Lorem ipsum Sed eiusmod esse aliqua sed incididunt aliqua incididunt mollit id et sit proident dolor nulla sed commodo est ad minim elit reprehenderit nisi officia aute incididunt velit sint in aliqua...
                        </p>
                    </div>
                    <div class="entry__meta">
                        <span class="entry__meta-links">
                            <a href="#">Health</a> 
                            <a href="#">Lifestyle</a>
                        </span>
                    </div>
                </div>

            </article> <!-- end article -->

            <article class="masonry__brick entry format-video" data-aos="fade-up">
                    
                <div class="entry__thumb video-image">
                    <a href="https://player.vimeo.com/video/117310401?color=01aef0&title=0&byline=0&portrait=0" data-lity>
                        <img src="images/thumbs/masonry/shutterbug-400.jpg" 
                                srcset="images/thumbs/masonry/shutterbug-400.jpg 1x, images/thumbs/masonry/shutterbug-800.jpg 2x" alt="">
                    </a>
                </div>

                <div class="entry__text">
                    <div class="entry__header">
                        
                        <div class="entry__date">
                            <a href="single-video.html">December 10, 2017</a>
                        </div>
                        <h1 class="entry__title"><a href="single-video.html">Key Benefits Of Family Photography.</a></h1>
                        
                    </div>
                    <div class="entry__excerpt">
                        <p>
                            Lorem ipsum Sed eiusmod esse aliqua sed incididunt aliqua incididunt mollit id et sit proident dolor nulla sed commodo est ad minim elit reprehenderit nisi officia aute incididunt velit sint in aliqua...
                        </p>
                    </div>
                    <div class="entry__meta">
                        <span class="entry__meta-links">
                            <a href="category.html">Family</a> 
                            <a href="category.html">Photography</a>
                        </span>
                    </div>
                </div>

            </article> <!-- end article -->


            <article class="masonry__brick entry format-gallery" data-aos="fade-up">
                    
                <div class="entry__thumb slider">
                    <div class="slider__slides">
                        <div class="slider__slide">
                            <img src="images/thumbs/masonry/gallery/gallery-1-400.jpg" 
                                    srcset="images/thumbs/masonry/gallery/gallery-1-400.jpg 1x, images/thumbs/masonry/gallery/gallery-1-800.jpg 2x" alt=""> 
                        </div>
                        <div class="slider__slide">
                            <img src="images/thumbs/masonry/gallery/gallery-2-400.jpg" 
                                    srcset="images/thumbs/masonry/gallery/gallery-2-400.jpg 1x, images/thumbs/masonry/gallery/gallery-2-800.jpg 2x" alt=""> 
                        </div>
                        <div class="slider__slide">
                            <img src="images/thumbs/masonry/gallery/gallery-3-400.jpg" 
                                    srcset="images/thumbs/masonry/gallery/gallery-3-400.jpg 1x, images/thumbs/masonry/gallery/gallery-3-800.jpg 2x" alt="">  
                        </div>
                    </div>
                </div>

                <div class="entry__text">
                    <div class="entry__header">
                        
                        <div class="entry__date">
                            <a href="single-gallery.html">December 10, 2017</a>
                        </div>
                        <h1 class="entry__title"><a href="single-gallery.html">Workspace Design Trends and Ideas.</a></h1>
                        
                    </div>
                    <div class="entry__excerpt">
                        <p>
                            Lorem ipsum Sed eiusmod esse aliqua sed incididunt aliqua incididunt mollit id et sit proident dolor nulla sed commodo est ad minim elit reprehenderit nisi officia aute incididunt velit sint in aliqua...
                        </p>
                    </div>
                    <div class="entry__meta">
                        <span class="entry__meta-links">
                            <a href="category.html">Work</a> 
                            <a href="category.html">Management</a>
                        </span>
                    </div>
                </div>

            </article> <!-- end article -->

            <article class="masonry__brick entry format-audio" data-aos="fade-up">

                <div class="entry__thumb">
                    <a href="single-audio.html" class="entry__thumb-link">
                        <img src="images/thumbs/masonry/guitarman-400.jpg" 
                                srcset="images/thumbs/masonry/guitarman-400.jpg 1x, images/thumbs/masonry/guitarman-800.jpg 2x" alt="">
                    </a>
                    <div class="audio-wrap">
                        <audio id="player" src="media/AirReview-Landmarks-02-ChasingCorporate.mp3" width="100%" height="42" controls="controls"></audio>
                    </div>
                </div>

                <div class="entry__text">
                    <div class="entry__header">
                        
                        <div class="entry__date">
                            <a href="single-audio.html">December 10, 2017</a>
                        </div>
                        <h1 class="entry__title"><a href="single-audio.html">What Your Music Preference Says About You and Your Personality.</a></h1>
                        
                    </div>
                    <div class="entry__excerpt">
                        <p>
                            Lorem ipsum Sed eiusmod esse aliqua sed incididunt aliqua incididunt mollit id et sit proident dolor nulla sed commodo est ad minim elit reprehenderit nisi officia aute incididunt velit sint in aliqua...
                        </p>
                    </div>
                    <div class="entry__meta">
                        <span class="entry__meta-links">
                            <a href="category.html">Music</a> 
                            <a href="category.html">Lifestyle</a>
                        </span>
                    </div>
                </div>

            </article> <!-- end article -->

            <article class="masonry__brick entry format-link" data-aos="fade-up">
                
                <div class="entry__thumb">
                    <div class="link-wrap">
                        <p>The Only Resource You Will Need To Start a Blog Using WordPress.</p>
                        <cite>
                            <a target="_blank" href="https://colorlib.com/">https://colorlib.com</a>
                        </cite>
                    </div>
                </div>
                
            </article> <!-- end article -->

            <article class="masonry__brick entry format-standard" data-aos="fade-up">

                <div class="entry__thumb">
                    <a href="single-standard.html" class="entry__thumb-link">
                        <img src="images/thumbs/masonry/jump-400.jpg" 
                                srcset="images/thumbs/masonry/jump-400.jpg 1x, images/thumbs/masonry/jump-800.jpg 2x" alt="">
                    </a>
                </div>

                <div class="entry__text">
                    <div class="entry__header">
                        
                        <div class="entry__date">
                            <a href="single-standard.html">December 10, 2017</a>
                        </div>
                        <h1 class="entry__title"><a href="single-standard.html">Create Meaningful Family Moments.</a></h1>
                        
                    </div>
                    <div class="entry__excerpt">
                        <p>
                            Lorem ipsum Sed eiusmod esse aliqua sed incididunt aliqua incididunt mollit id et sit proident dolor nulla sed commodo est ad minim elit reprehenderit nisi officia aute incididunt velit sint in aliqua...
                        </p>
                    </div>
                    <div class="entry__meta">
                        <span class="entry__meta-links">
                            <a href="category.html">Family</a>
                            <a href="category.html">Relationship</a>
                        </span>
                    </div>
                </div>

            </article> <!-- end article -->

            <article class="masonry__brick entry format-standard" data-aos="fade-up">

                <div class="entry__thumb">
                    <a href="single-standard.html" class="entry__thumb-link">
                        <img src="images/thumbs/masonry/beetle-400.jpg" 
                                srcset="images/thumbs/masonry/beetle-400.jpg 1x, images/thumbs/masonry/beetle-800.jpg 2x" alt="">
                    </a>
                </div>

                <div class="entry__text">
                    <div class="entry__header">
                        
                        <div class="entry__date">
                            <a href="single-standard.html">December 10, 2017</a>
                        </div>
                        <h1 class="entry__title"><a href="single-standard.html">Throwback To The Good Old Days.</a></h1>
                        
                    </div>
                    <div class="entry__excerpt">
                        <p>
                            Lorem ipsum Sed eiusmod esse aliqua sed incididunt aliqua incididunt mollit id et sit proident dolor nulla sed commodo est ad minim elit reprehenderit nisi officia aute incididunt velit sint in aliqua...
                        </p>
                    </div>
                    <div class="entry__meta">
                        <span class="entry__meta-links">
                            <a href="category.html">Lifestyle</a>
                        </span>
                    </div>
                </div>

            </article> <!-- end article -->

            <article class="masonry__brick entry format-standard" data-aos="fade-up">

                <div class="entry__thumb">
                    <a href="single-standard.html" class="entry__thumb-link">
                        <img src="images/thumbs/masonry/fuji-400.jpg" 
                                srcset="images/thumbs/masonry/fuji-400.jpg 1x, images/thumbs/masonry/fuji-800.jpg 2x" alt="">
                    </a>
                </div>

                <div class="entry__text">
                    <div class="entry__header">
                        
                        <div class="entry__date">
                            <a href="single-standard.html">December 10, 2017</a>
                        </div>
                        <h1 class="entry__title"><a href="single-standard.html">Just Another  Standard Format Post.</a></h1>
                        
                    </div>
                    <div class="entry__excerpt">
                        <p>
                            Lorem ipsum Sed eiusmod esse aliqua sed incididunt aliqua incididunt mollit id et sit proident dolor nulla sed commodo est ad minim elit reprehenderit nisi officia aute incididunt velit sint in aliqua...
                        </p>
                    </div>
                    <div class="entry__meta">
                        <span class="entry__meta-links">
                            <a href="category.html">Design</a> 
                            <a href="category.html">Photography</a>
                        </span>
                    </div>
                </div>

            </article> <!-- end article -->

            <article class="masonry__brick entry format-standard" data-aos="fade-up">

                <div class="entry__thumb">
                    <a href="single-standard.html" class="entry__thumb-link">
                        <img src="images/thumbs/masonry/sydney-400.jpg" 
                                srcset="images/thumbs/masonry/sydney-400.jpg 1x, images/thumbs/masonry/sydney-800.jpg 2x" alt="">
                    </a>
                </div>

                <div class="entry__text">
                    <div class="entry__header">
                        
                        <div class="entry__date">
                            <a href="single-standard.html">December 10, 2017</a>
                        </div>
                        <h1 class="entry__title"><a href="single-standard.html">Planning Your First Trip to Sydney.</a></h1>
                        
                    </div>
                    <div class="entry__excerpt">
                        <p>
                            Lorem ipsum Sed eiusmod esse aliqua sed incididunt aliqua incididunt mollit id et sit proident dolor nulla sed commodo est ad minim elit reprehenderit nisi officia aute incididunt velit sint in aliqua...
                        </p>
                    </div>
                    <div class="entry__meta">
                        <span class="entry__meta-links">
                            <a href="category.html">Travel</a> 
                            <a href="category.html">Vacation</a>
                        </span>
                    </div>
                </div>

            </article> <!-- end article -->

        </div> <!-- end masonry -->
    </div> <!-- end masonry-wrap -->
    </div>
    </div>
    </div>
    </div>



    <!-- /.section-space80 -->
    <div class="section-space60 team" id="team">
        <div class="container">
        <div class="team-head text-center">
        <h3>SUPERB TRAINERS</h3>
        <span> </span>
    </div>
    <div class="team-grids">
        <div class="col-md-3 team-grid">
            <a data-toggle="modal" data-target=".bs-example-modal-md" href="#" class="b-link-stripe b-animate-go  thickbox">
             <img class="p-img" src="images/t1.jpg" /><div class="b-wrapper">
                 <h2 class="b-animate b-from-left    b-delay03 ">
                     <div class="animate-head">
                         <div class="animate-head-left">
                             <h3>Duis nec congue</h3>
                             <span>Neque porro quisquam est qui dolorem </span>
                         </div>
                         <div class="clearfix"> </div>
                     </div>
                 </h2>
              </div></a>
              <div class="t-member-info">
                  <h5>Otto W. Schreffler</h5>
                  <span>trainer</span>
              </div>
        </div>
        <div class="col-md-3 team-grid">
            <a data-toggle="modal" data-target=".bs-example-modal-md" href="#" class="b-link-stripe b-animate-go  thickbox">
             <img class="p-img" src="images/t2.jpg" /><div class="b-wrapper">
                 <h2 class="b-animate b-from-left    b-delay03 ">
                     <div class="animate-head">
                         <div class="animate-head-left">
                             <h3>Duis nec congue</h3>
                             <span>Neque porro quisquam est qui dolorem </span>
                         </div>
                         <div class="clearfix"> </div>
                     </div>
                 </h2>
              </div></a>
              <div class="t-member-info">
                  <h5>Brooke C. Carrington</h5>
                  <span>trainer</span>
              </div>
        </div>
        <div class="col-md-3 team-grid">
            <a data-toggle="modal" data-target=".bs-example-modal-md" href="#" class="b-link-stripe b-animate-go  thickbox">
             <img class="p-img" src="images/t3.jpg" /><div class="b-wrapper">
                 <h2 class="b-animate b-from-left    b-delay03 ">
                     <div class="animate-head">
                         <div class="animate-head-left">
                             <h3>Duis nec congue</h3>
                             <span>Neque porro quisquam est qui dolorem </span>
                         </div>
                         <div class="clearfix"> </div>
                     </div>
                 </h2>
              </div></a>
              <div class="t-member-info">
                  <h5>Arthur M. Huffman</h5>
                  <span>trainer</span>
              </div>
        </div>
        <div class="col-md-3 team-grid">
            <a data-toggle="modal" data-target=".bs-example-modal-md" href="#" class="b-link-stripe b-animate-go  thickbox">
             <img class="p-img" src="images/t4.jpg" /><div class="b-wrapper">
                 <h2 class="b-animate b-from-left    b-delay03 ">
                     <div class="animate-head">
                         <div class="animate-head-left">
                             <h3>Bernice B. Work</h3>
                             <span>Neque porro quisquam est qui dolorem </span>
                         </div>
                         <div class="clearfix"> </div>
                     </div>
                 </h2>
              </div></a>
              <div class="t-member-info">
                  <h5>Otto W. Schreffler</h5>
                  <span>trainer</span>
              </div>
        </div>
    </div>
        </div>
    </div>
    <!-- /.section-space80 -->
    <div class="section-space80 bg-light">
        <!-- section space -->
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 text-center">
                    <div class="mb60 section-title">
                        <h1>Yoga Latest News</h1>
                        <p>Read latest news from yoga studio lorem ipsum sitamet dolor.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <div class="post-holder mb40">
                        <div class="post-img">
                            <a href="blog-single.html" class="imghover"><img src="images/post-img-1.jpg" alt="" class="img-responsive"></a>
                        </div>
                        <div class="post-block pinside30 outline bg-white">
                            <!-- /.post meta -->
                            <div class="post-header">
                                <div class="post-meta mb30">
                                    <!-- post meta -->
                                    <span class="meta-date"><i class="fa fa-calendar"></i> 25 April, 2017 </span>
                                    <span class="meta-comment"><i class="fa fa-comments"></i> <a href="#" class="meta-link">03 Comments </a></span>
                                </div>
                                <h2 class="post-title"><a href="blog-single.html" class="title">Tips To Maintain Your ‘Yoga High’</a></h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="post-holder mb40">
                        <div class="post-img">
                            <a href="blog-single.html" class="imghover"><img src="images/post-img-2.jpg" alt="" class="img-responsive"></a>
                        </div>
                        <div class="post-block pinside30 outline bg-white">
                            <!-- /.post meta -->
                            <div class="post-header">
                                <div class="post-meta mb30">
                                    <!-- post meta -->
                                    <span class="meta-date"><i class="fa fa-calendar"></i> 25 April, 2017 </span>
                                    <span class="meta-comment"><i class="fa fa-comments"></i> <a href="#" class="meta-link">03 Comments </a></span>
                                </div>
                                <h2 class="post-title"><a href="blog-single.html" class="title">Relax More, Metabolize Better</a></h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="post-holder mb40">
                        <div class="post-img">
                            <a href="blog-single.html" class="imghover"><img src="images/post-img-3.jpg" alt="" class="img-responsive"></a>
                        </div>
                        <div class="post-block pinside30 outline bg-white">
                            <!-- /.post meta -->
                            <div class="post-header">
                                <div class="post-meta mb30">
                                    <!-- post meta -->
                                    <span class="meta-date"><i class="fa fa-calendar"></i> 25 April, 2017 </span>
                                    <span class="meta-comment"><i class="fa fa-comments"></i> <a href="#" class="meta-link">03 Comments </a></span>
                                </div>
                                <h2 class="post-title"><a href="blog-single.html" class="title">Way to Boost Your Immunity</a></h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-space80">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-1 col-md-10 col-sm-12">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="classes-logo mb60">
                                <img src="images/classes-logo.png" alt="" class="">
                            </div>
                            <h1 class="classes-title mb40">Choose Your Classes and<br>Start Your Training</h1>
                            <p class="classes-content mb40">Curabitur venenatis pulvinar sem quis lorem ipsum sitamet dolor sitamet domec voinose renoialiquam nibh.</p>
                            <a href="contact-us.html" class="btn btn-outline">contact us</a>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="trainers-img">
                                <img src="images/trainers1.png" class="" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section-space80 bg-light">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-1 col-md-10 col-sm-12">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 text-center mb60">
                            <h1>Student Reviews</h1>
                            <p>Maecenas gravidaorciet ullamcorper lobortis libero mauris rutrum quam.</p>
                        </div>
                    </div>
                    <div class="text-center pinside30 bg-white outline">
                        <div class="owl-carousel owl-theme testimonial-carousel">
                            <div class="owl-item">
                                <div class="testimonial-img mb30">
                                    <img src="images/testimonial1.jpg" class="img-responsive img-circle" alt="">
                                </div>
                                <div class="testimonial-info">
                                    <p class="testimonial-content">“I would highly recommend this unit of study for all who have the desire to understand
                                        <br> the joy that yoga practice brings to ones life and beyond Yoga is something
                                        <br> I look forward to in my week”</p>
                                    <span class="testimonial-text">- Sarah Montes</span>
                                    <span class="testimonial-meta">(Student)</span>
                                </div>
                            </div>
                            <div class="owl-item">
                                <div class="testimonial-img mb30">
                                    <img src="images/testimonial2.jpg" class="img-responsive img-circle" alt="">
                                </div>
                                <div class="testimonial-info">
                                    <p class="testimonial-content">“The experience of my first class was like jumping and looking over a wall. Once I saw what was over that wall, I knew I had to go over and experience the other side. Thank you for guiding me through a journey. </p>
                                    <span class="testimonial-text">- Kathleen Wagner</span>
                                    <span class="testimonial-meta">(Student)</span>
                                </div>
                            </div>
                            <div class="owl-item">
                                <div class="testimonial-img mb30">
                                    <img src="images/testimonial3.jpg" class="img-responsive img-circle" alt="">
                                </div>
                                <div class="testimonial-info">
                                    <p class="testimonial-content">“It’s very difficult to find a good yoga class. From the moment I walked into yoga studio I knew this was what I had been looking for. you made me feel very welcome, I highly recommend you.</p>
                                    <span class="testimonial-text">- Edith Thompson</span>
                                    <span class="testimonial-meta">(Student)</span>
                                </div>
                            </div>
                            <div class="owl-item">
                                <div class="testimonial-img mb30">
                                    <img src="images/testimonial4.jpg" class="img-responsive img-circle" alt="">
                                </div>
                                <div class="testimonial-info">
                                    <p class="testimonial-content"> I was apprehensive about joining a class. you guys make me feel so welcome and I have since found out. it is not just the first lesson or two when shows you lovely caring attitude, it is evident in every class.</p>
                                    <span class="testimonial-text">- Corey Imhoff</span>
                                    <span class="testimonial-meta">(Student)</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer section-space60">
        <!-- footer -->
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4">
                    <div class="footer-logo"> <img src="images/logo.png" alt="" class=""> </div>
                </div>
                <div class="col-md-9 col-sm-8">
                    <p>Based in Gujarat, yoga studio is india’s leading yoga studio offering over 40 per classes per week across
                        <br> four locations in Mumbai, Delhi, Ahemedabad and Rajkot.</p>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-3 col-sm-4">
                    <div class="footer-widget">
                        <!-- footer-widget -->
                        <i class="fa fa-map-marker mb30"></i>
                        <h3 class="footer-title">Locations</h3>
                        <address>
                            Jacob street estate,
                            <br> california 8007, America
                        </address>
                        <ul class="listnone contact">
                            <li>+91-123-456-789</li>
                            <li>+91-456-369-964</li>
                        </ul>
                    </div>
                    <!-- /.footer-widget -->
                </div>
                <div class="col-md-3 col-sm-4">
                    <div class="footer-widget">
                        <!-- footer-widget -->
                        <i class="fa fa-clock-o mb30"></i>
                        <h3 class="footer-title">Opening Hour</h3>
                        <ul class="listnone mb0 no-padding">
                            <li>Mon: 6:00am to 9:00pm</li>
                            <li>Tues - Fri: 8:00am to 7:00pm</li>
                            <li>Sat - Sun: 9:00am 1:00pm </li>
                        </ul>
                    </div>
                    <!-- /.footer-widget -->
                </div>
                <div class="col-md-2 col-sm-4">
                    <div class="footer-widget">
                        <!-- footer-widget -->
                        <i class="fa fa-mail-forward mb30"></i>
                        <h3 class="footer-title">Follow Us On</h3>
                        <ul class="listnone no-padding mb0">
                            <li class="footer-link"><a href="#"><i class="fa fa-facebook-f"></i> facebook</a></li>
                            <li class="footer-link"><a href="#"><i class="fa fa-twitter"></i> twitter</a></li>
                            <li class="footer-link"><a href="#"><i class="fa fa-google-plus"></i> google-plus</a></li>
                        </ul>
                    </div>
                    <!-- /.footer-widget -->
                </div>
                <div class="col-md-4 col-sm-12">
                    <div class="footer-widget">
                        <!-- footer-widget -->
                        <i class="fa fa-envelope mb30"></i>
                        <h3 class="footer-title">Newsletter</h3>
                        <form>
                            <div class="form-group">
                                <label for="inputEmail3" class="sr-only control-label">Email</label>
                                <input type="email" class="form-control" id="inputEmail3" placeholder="Enter your email address">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-outline">Subscribe Now</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.footer-widget -->
                </div>
            </div>
        </div>
    </div>
    <!-- /.footer -->
    <div class="tiny-footer">
        <!-- tiny-footer -->
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <p>Yoga Studio © Copy Rights 2018. All Rights Reserved.</p>
                </div>
            </div>
        </div>
    </div>
    <!-- /.tiny-footer -->
    <!-- back to top icon -->
    <a href=" #0 " class="cd-top" title="Go to top">Top</a>
    <div class="modal  " id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content ">
                <div class="modal-body">
                    <iframe width="100%" height="600" src="https://www.youtube.com/embed/CoirzH4fByQ" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/menumaker.js"></script>
    <!-- animsition -->
    <script type="text/javascript" src="js/animsition.js"></script>
    <script type="text/javascript" src="js/animsition-script.js"></script>
    <!-- sticky header -->
    <script type="text/javascript" src="js/jquery.sticky.js"></script>
    <script type="text/javascript" src="js/sticky-header.js"></script>
    <!-- owl carsoul -->
    <script type="text/javascript" src="js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="js/testimonial.js"></script>
    <!-- Back to top script -->
    <script src="js/back-to-top.js" type="text/javascript"></script>

        <!-- Java Script
    ================================================== -->
    <script src="js/plugins.js"></script>
    <script src="js/main.js"></script>
    <script src="js/jquery-3.2.1.min.js"></script>
</body>

</html>
