<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    // public function getHome(){
    //     return view('home');
    // }

    public function getIndex(){
        return view('index');
    }

    public function getSchedule(){
        return view('schedule');
    }

    public function getCrosstraining(){
        return view('crosstraining');
    }
    
    public function getPersonaltraining(){
        return view('personaltraining');
    }

    public function getKickboxing(){
        return view('kickboxing');
    }

    public function getContact(){
        return view('contact');
    }

    public function getGallery(){
        return view('gallery');
    }
}
