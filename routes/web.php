<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','Controller@getIndex');

Route::get('home','Controller@getIndex');

Route::get('schedule','Controller@getSchedule');

Route::get('crosstraining','Controller@getCrosstraining');

Route::get('personaltraining','Controller@getPersonaltraining');

Route::get('kickboxing','Controller@getKickboxing');

Route::get('yoga','Controller@getYoga');

Route::get('contact','Controller@getContact');

Route::get('gallery','Controller@getGallery');


